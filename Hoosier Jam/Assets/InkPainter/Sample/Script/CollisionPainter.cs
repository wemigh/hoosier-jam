﻿using UnityEngine;

namespace Es.InkPainter.Sample
{
	[RequireComponent(typeof(Collider), typeof(MeshRenderer))]
	public class CollisionPainter : MonoBehaviour
	{
		[SerializeField]
		private Brush brush = null;

		[SerializeField]
		private int wait = 3;

		private int waitCount;

        private bool eraseFlag = false;

		public void Awake()
		{
			GetComponent<MeshRenderer>().material.color = brush.Color;
		}

        private void Update()
        {
            if (Input.GetButtonDown("Jump"))
            {
                eraseFlag = !eraseFlag;
            }
        }

        public void FixedUpdate()
		{
			++waitCount;
		}

		public void OnCollisionStay(Collision collision)
		{
			if(waitCount < wait)
				return;
			waitCount = 0;

			foreach(var p in collision.contacts)
			{
				var canvas = p.otherCollider.GetComponent<InkCanvas>();
                if (canvas != null)
                {
                    if (eraseFlag) {
                        canvas.Erase(brush, p.point);
                    }
                    else
                    {
                        canvas.Paint(brush, p.point);
                    }
                }
			}
		}
	}
}