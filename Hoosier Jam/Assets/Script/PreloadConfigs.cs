﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkConfig;

public class PreloadConfigs : MonoBehaviour {

	public GameObject profanity_prefab;

	// Use this for initialization
	void Start () {
		UnityPlatform.Setup();
		Config.FileManager.AddSource(new ResourcesSource(hotload: true));
		Config.Preload();
		Config.OnPreload += () => {
			Instantiate(profanity_prefab, new Vector3(0, 0, 0), Quaternion.identity);
		};
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
