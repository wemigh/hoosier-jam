﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkConfig;
using TMPro;
using DG.Tweening;

public class ProfanityController : MonoBehaviour {

	public string Name;
	public List<string> First; 
	public List<string> DwarfWords;

	bool is_loaded = false;

	public TextMeshProUGUI curse_display;

	// Use this for initialization
	void Start () {
		UnityPlatform.Setup();
		Config.FileManager.AddSource(new ResourcesSource(hotload: true));
		Config.Preload();
		Config.OnPreload += () => {
			Config.ApplyThis("yelling", this);

			is_loaded = true;

			DwarfCurse();
		};
		
		

		
	}
	

	public void Curse (Vector3 location) {
		transform.position = location;
		
		transform.localScale = Vector3.one;
		curse_display.alpha = 1f;
		curse_display.text = DwarfCurse();

		transform.DOKill();
		Tweener tempTween = transform.DOScale(1.2f, 1);
		tempTween.OnComplete(HideProfanity);

	}

	void HideProfanity() {
		curse_display.alpha = 0;
	}
	// Update is called once per frame
	void Update () {
		transform.LookAt(Camera.main.transform);
	}

	string DwarfCurse() {
		int num = Random.Range(2,6);

		string curse = "";

		for(int x = 0; x < num; x++ ) {
			if (curse != ""){
				curse += " ";
			}

			curse += DwarfWords[Random.Range(0, DwarfWords.Count)];
		}

		int num_exclamations = Random.Range(1, 5);

		for (int i = 0; i < num_exclamations; i ++){
			curse += "!";
		}

		return curse;

	}
}
