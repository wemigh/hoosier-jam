﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DwarfMovement : MonoBehaviour {

    public float speed;
    public float rotSpeed;

    Rigidbody rb;
    public float angle = 0f;

    float collisionCool = 0f;
    public float decay = .8f;


    ProfanityController profanity;
    public TextMeshProUGUI timerText;
    public float timerFull = 60f;
    float timerStart = 0f;

    bool gameOver = false;

    public Animator myAnim;


    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();

        profanity = Transform.FindObjectOfType<ProfanityController>();
        timerStart = Time.time;

	}

    // Update is called once per frame
    void Update() {
        rb.velocity += transform.forward * speed * Time.deltaTime;

        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        SetRot(input.x * rotSpeed * Time.deltaTime);

        if (collisionCool > 0f)
            collisionCool -= Time.deltaTime;

        TimerUpdate();

        myAnim.speed = rb.velocity.magnitude * .05f;

        }


    void TimerUpdate()
    {
        if (gameOver == false)
        {
            float timer = Time.time - timerStart;
            timer = timerFull - timer;

            if (timer < 0)
            {
                gameOver = true;
                print("GAME OVER!");
                timer = 0f;
            }

            int timerRound = (int)(timer);
            timerText.text = timerRound.ToString();

        }


    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collisionCool > 0f)
            return;

        if (collision.impulse.magnitude > 0f)//If a large enough hit
        {

            Vector3 wallForward = collision.other.transform.forward;
            //Flip direction
            float dot = Vector3.Dot(wallForward, rb.velocity.normalized);

            rb.velocity += dot * wallForward * rb.velocity.magnitude * decay;// rb.velocity.magnitude * Vector3.Lerp(rb.velocity.normalized, -collision.impulse.normalized, Mathf.Abs(dot));

            LookToSpeed();

            // SWEAR!!!
            
            
            
            if( profanity != null ){
                profanity.Curse(transform.position);
            }


            /*
            float setAngle = Vector3.Angle(Vector3.forward, rb.velocity.normalized);
            if (rb.velocity.normalized.z > 0f)
                setAngle = -setAngle;
            setAngle = setAngle % 360f;

            print("setAngle: " + setAngle+ "End velocity: " + rb.velocity);
            setAngle = setAngle - angle;
            SetRot(setAngle);
            print("angle: " + angle );

            
            */
            collisionCool = .1f;
            //UnityEditor.EditorApplication.isPaused = true;
        }
    }

    void LookToSpeed()
    {
        Quaternion look = Quaternion.LookRotation(rb.velocity.normalized, Vector3.up);
        angle = look.eulerAngles.y;

        if (angle < 0)
            angle = 360f + angle;

        if (angle > 360f)
            angle = angle % 360f;

        transform.rotation = look;

    }

    void SetRot(float delta)
    {
        angle += delta;

        if (angle < 0)
            angle = 360f + angle;

        if (angle > 360f)
            angle = angle % 360f;


        transform.rotation = Quaternion.Euler(new Vector3(0f, angle, 0f));
    }
}
