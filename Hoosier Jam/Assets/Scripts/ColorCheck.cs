﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ColorCheck : MonoBehaviour {

    public TextMeshProUGUI colorText;

    RenderTexture rt;
    Texture2D tex;

    float timeLeft = 0;
    // The calibration tube is about 52,000 but it's slightly larger than the playing field 
    int solidCount = 50000;

    // Use this for initialization
    void Start () {
        rt = GetComponent<Camera>().targetTexture;
        tex = new Texture2D(rt.width, rt.height, TextureFormat.RGB24, false);
    }
	
	// Update is called once per frame
	void Update () {
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0)
        {
            timeLeft = 0.1f;
            int count = 0;

            RenderTexture.active = rt;
            tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
            Color32[] pixels = tex.GetPixels32();

            for (int i = 0; i < pixels.Length; i++)
            {
                if (pixels[i].g == 255)
                {
                    count++;
                }
            }

            float pct = count;
            pct /= solidCount;
            
            colorText.text = Mathf.FloorToInt(pct * 100)+"%";
        }
	}
}
