﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Es.InkPainter;

public class Test : MonoBehaviour {

    public Brush brush;

	// Use this for initialization
	void Start () {
        GetComponent<InkCanvas>().Paint(brush, new Vector3(0, 0, 0));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
