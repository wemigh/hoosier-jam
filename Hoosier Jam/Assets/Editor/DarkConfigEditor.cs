﻿using UnityEngine;
using UnityEditor;
using DarkConfig;

public class DarkConfigEditorMenus
{
    [MenuItem("Assets/DarkConfig/Autogenerate Index")]
    static void MenuGenerateIndex()
    {
        EditorUtils.GenerateIndex("/Resources/Configs");
        AssetDatabase.Refresh();
    }
}